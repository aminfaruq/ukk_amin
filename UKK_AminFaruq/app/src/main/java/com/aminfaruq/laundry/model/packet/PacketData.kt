package com.aminfaruq.laundry.model.packet

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PacketData (
    val note: String? = null,
    val price: String? = null,
    val name: String? = null,
    val id: String? = null
): Parcelable