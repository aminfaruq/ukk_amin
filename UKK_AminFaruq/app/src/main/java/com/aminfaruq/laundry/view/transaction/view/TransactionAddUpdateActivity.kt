package com.aminfaruq.laundry.view.transaction.view

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import androidx.appcompat.app.ActionBar
import android.widget.ListPopupWindow
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aminfaruq.laundry.R
import com.aminfaruq.laundry.model.packet.PacketData
import com.aminfaruq.laundry.model.transaction.TransactionData
import com.aminfaruq.laundry.view.transaction.viewModel.TransactionVM
import kotlinx.android.synthetic.main.activity_transaction_add_update.*

class TransactionAddUpdateActivity : AppCompatActivity(), View.OnClickListener {

    private var data: TransactionData? = null

    private var paymentStatus: String? = null
    private var progressStatus: String? = null
    private var packetId: String? = null

    private var listPacket: ArrayList<PacketData>? = null

    private lateinit var transactionVM: TransactionVM


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transaction_add_update)
        transactionVM = ViewModelProviders.of(this).get(TransactionVM::class.java)
        data = intent.getParcelableExtra(TransactionActivity.EXTRA_DATA)
        listPacket = intent.getParcelableArrayListExtra(TransactionActivity.EXTRA_PACKET)
        updateUI()
        setupAppbar()
        btnAddUpdate.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        handleGroupButton()
        initPopUpPacketList()
        observable()
    }

    private fun handleGroupButton() {
        btnGroupPayment.addOnButtonCheckedListener { group, checkedId, isChecked ->
            if (isChecked) {
                //'Dibayar','Belum_Dibayar'
                when (checkedId) {
                    R.id.btnPaymentPaid -> paymentStatus = "Dibayar"
                    R.id.btnPaymentNotYet -> paymentStatus = "Belum_Dibayar"
                }
            }

        }
        btnGroupProgress.addOnButtonCheckedListener { group, checkedId, isChecked ->
            if (isChecked) {
                when (checkedId) {
                    R.id.btnProgressNew -> progressStatus = "Baru"
                    R.id.btnProgressProcess -> progressStatus = "Proses"
                    R.id.btnProgressDone -> progressStatus = "Selesai"
                    R.id.btnProgressTaken -> progressStatus = "Diambil"

                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun updateUI() {
        if (data != null) {
            etCustomerName.setText(data?.customer_name)
            etQty.setText(data?.qty)
            etTotalPrice.setText(data?.total_price)
            btnAddUpdate.text = "Update Transaksi"
            progressStatus = data?.status_progress
            when (progressStatus) {
                "Baru" -> btnGroupProgress.check(R.id.btnProgressNew)
                "Proses" -> btnGroupProgress.check(R.id.btnProgressProcess)
                "Selesai" -> btnGroupProgress.check(R.id.btnProgressDone)
                "Diambil" -> btnGroupProgress.check(R.id.btnProgressTaken)
            }
            paymentStatus = data?.status_payment
            when (paymentStatus) {
                "Dibayar" -> btnGroupPayment.check(R.id.btnPaymentPaid)
                else -> btnGroupPayment.check(R.id.btnPaymentNotYet)
            }

            packetId = data?.id_packet

            listPacket?.forEach {
                if (packetId == it.id) {
                    btnPacket.text = it.name
                }
            }

        } else {

            btnAddUpdate.text = "Tambah Transaksi"
            btnDelete.visibility = View.GONE
        }
    }

    private fun setupAppbar() {
        val actionbar: ActionBar? = supportActionBar
        actionbar?.title = if (data == null) "Add Transaksi" else "Update Transaksi"
        actionbar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnAddUpdate -> {
                if (data == null) {
                    addData()
                } else {
                    updateData()
                }
            }

            R.id.btnDelete -> {
                transactionVM.deleteTransaction(data?.id.toString())
                finish()
                setResult(RESULT_OK, intent)
            }

        }
    }

    private fun addData() {
        transactionVM.addTransaction(
            etCustomerName.text.toString(),
            etQty.text.toString(),
            progressStatus ?: "",
            paymentStatus ?: "",
            etTotalPrice.text.toString(),
            packetId ?: ""
        )
        finish()
        setResult(RESULT_OK, intent)
    }

    private fun updateData() {
        transactionVM.updateTransaction(
            data?.id.toString(),
            etCustomerName.text.toString(),
            etQty.text.toString(),
            progressStatus ?: "",
            paymentStatus ?: "",
            etTotalPrice.text.toString(),
            packetId ?: ""
        )
        finish()
        setResult(RESULT_OK, intent)
    }


    private fun initPopUpPacketList() {
        val items = mutableListOf<String>()

        listPacket?.forEach {
            it.name?.let { it1 -> items.add(it1) }
        }

        val listPopWindow = ListPopupWindow(this, null, R.attr.listPopupWindowStyle)
        listPopWindow.anchorView = btnPacket
        val adapter = ArrayAdapter(this, R.layout.list_popup_window_item, items)
        listPopWindow.setAdapter(adapter)

        listPopWindow.setOnItemClickListener { parent, view, position, id ->
            btnPacket.text = listPacket?.get(position)?.name
            packetId = listPacket?.get(position)?.id
            listPopWindow.dismiss()
        }

        btnPacket.setOnClickListener {
            Log.d("Transaksi Pop up", listPacket?.size.toString())
            listPopWindow.show()
        }


    }

    private fun observable() {
        transactionVM.observeAddTransaction().observe(this, Observer {
            Toast.makeText(
                this@TransactionAddUpdateActivity,
                it.message,
                Toast.LENGTH_SHORT
            ).show()
        })

        transactionVM.observeUpdateTransaction().observe(this, Observer {
            Toast.makeText(
                this@TransactionAddUpdateActivity,
                it.message,
                Toast.LENGTH_SHORT
            ).show()
        })

        transactionVM.observeDeleteTransaction().observe(this, Observer {
            Toast.makeText(
                this@TransactionAddUpdateActivity,
                it.message,
                Toast.LENGTH_SHORT
            ).show()
        })
    }

}