package com.aminfaruq.laundry.view.admin.viewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aminfaruq.laundry.model.login.UserData
import com.aminfaruq.laundry.network.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AdminVM : ViewModel() {
    val userList = MutableLiveData<ArrayList<UserData>>()

    internal fun setUser() {
        val apiClient = ApiClient.create()
        apiClient.getUser()
            .enqueue(object : Callback<UserData.UserResponse> {

                override fun onResponse(
                    call: Call<UserData.UserResponse>,
                    response: Response<UserData.UserResponse>
                ) {
                    if (response.body() != null) {
                        val response: ArrayList<UserData> =
                            response.body()?.data as ArrayList<UserData>
                        userList.value = response
                    }
                }

                override fun onFailure(call: Call<UserData.UserResponse>, t: Throwable) {
                    Log.e("setUser" , t.message.toString())
                }
            })

    }

    internal fun getUser(): LiveData<ArrayList<UserData>> {
        return userList
    }

}