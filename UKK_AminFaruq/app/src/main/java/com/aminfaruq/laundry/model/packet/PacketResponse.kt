package com.aminfaruq.laundry.model.packet

data class PacketResponse (
    val data: MutableList<PacketData>? = null,
    val message: String? = null,
    val isSuccess: Boolean? = null
)