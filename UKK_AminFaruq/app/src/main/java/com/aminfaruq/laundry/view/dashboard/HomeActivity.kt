package com.aminfaruq.laundry.view.dashboard

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.aminfaruq.laundry.R
import com.aminfaruq.laundry.model.login.UserData
import com.aminfaruq.laundry.view.admin.view.AdminActivity
import com.aminfaruq.laundry.view.login.view.LoginActivity.Companion.EXTRA_DATA
import com.aminfaruq.laundry.view.packet.view.PacketActivity
import com.aminfaruq.laundry.view.transaction.view.TransactionActivity
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() , View.OnClickListener{
    private var user: UserData? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        supportActionBar?.hide()
        user = intent.getParcelableExtra(EXTRA_DATA)
        tvName.text = user?.name

        btnTransaction.setOnClickListener(this)
        btnAdmin.setOnClickListener(this)
        btnPacket.setOnClickListener(this)
        btnReport.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnTransaction -> {
                val intent = Intent(this, TransactionActivity::class.java)
                startActivity(intent)
            }

            R.id.btnAdmin -> {
                val intent = Intent(this, AdminActivity::class.java)
                startActivity(intent)
            }

            R.id.btnPacket -> {
                val intent = Intent(this, PacketActivity::class.java)
                startActivity(intent)
            }

            R.id.btnReport -> {
                Toast.makeText(this , "Sorry the feature still in develop" , Toast.LENGTH_SHORT).show()
            }
            else -> Toast.makeText(this , "Have a something?" , Toast.LENGTH_SHORT).show()
        }
    }

}