package com.aminfaruq.laundry.view.packet.view

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.aminfaruq.laundry.R
import com.aminfaruq.laundry.model.packet.PacketData
import com.aminfaruq.laundry.view.packet.adapter.PacketAdapter
import com.aminfaruq.laundry.view.packet.viewModel.PacketVM
import kotlinx.android.synthetic.main.activity_packet.*


class PacketActivity : AppCompatActivity() {

    companion object {
        var EXTRA_DATA = "extra_data"
    }

    private lateinit var packetVM: PacketVM
    private lateinit var adapter: PacketAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_packet)

        packetVM = ViewModelProviders.of(this).get(PacketVM::class.java)
        packetVM.getPacket()
        showData()

        fab.setOnClickListener {
            val intent = Intent(this, PacketAddUpdateActivity::class.java)
            startActivity(intent)
        }
    }


    private fun showData() {
        adapter =
            PacketAdapter(object :
                PacketAdapter.OnItemClick {
                override fun onClick(item: PacketData) {
                    // your code in here
                    val intent = Intent(this@PacketActivity, PacketAddUpdateActivity::class.java)
                    intent.putExtra(EXTRA_DATA, item)
                    startActivity(intent)
                }
            })
        adapter.notifyDataSetChanged()

        rv_packet.layoutManager = LinearLayoutManager(this)
        rv_packet.adapter = adapter
        packetVM.observeGetPacket().observe(this, Observer {
            adapter.setData(it)
            packetVM.getPacket()
        })

    }

}