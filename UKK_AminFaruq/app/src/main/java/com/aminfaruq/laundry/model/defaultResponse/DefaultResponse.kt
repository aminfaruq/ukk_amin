package com.aminfaruq.laundry.model.defaultResponse

data class DefaultResponse(
    val message: String? = null,
    val isSuccess: Boolean? = null
)