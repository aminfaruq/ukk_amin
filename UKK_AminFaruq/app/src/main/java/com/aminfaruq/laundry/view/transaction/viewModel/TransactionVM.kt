package com.aminfaruq.laundry.view.transaction.viewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aminfaruq.laundry.model.defaultResponse.DefaultResponse
import com.aminfaruq.laundry.model.packet.PacketData
import com.aminfaruq.laundry.model.packet.PacketResponse
import com.aminfaruq.laundry.model.transaction.TransactionData
import com.aminfaruq.laundry.model.transaction.TransactionResponse
import com.aminfaruq.laundry.network.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TransactionVM : ViewModel() {

    private val listTransaction = MutableLiveData<ArrayList<TransactionData>>()
    private val listPacket = MutableLiveData<ArrayList<PacketData>>()
    private val addTransaction = MutableLiveData<DefaultResponse>()
    private val updateTransaction = MutableLiveData<DefaultResponse>()
    private val deleteTransaction = MutableLiveData<DefaultResponse>()


    internal fun getTransaction() {
        val apiClient = ApiClient.create()
        apiClient.getTransaction()
            .enqueue(object : Callback<TransactionResponse> {
                override fun onResponse(
                    call: Call<TransactionResponse>,
                    response: Response<TransactionResponse>
                ) {
                    if (response.body() != null) {
                        val response: ArrayList<TransactionData> =
                            response.body()?.data as ArrayList<TransactionData>
                        listTransaction.value = response
                    }
                }

                override fun onFailure(call: Call<TransactionResponse>, t: Throwable) {
                    Log.e("setPacket", t.message.toString())
                }
            })
    }

    internal fun getPacket() {
        val apiClient = ApiClient.create()
        apiClient.getPacket()
            .enqueue(object : Callback<PacketResponse> {
                override fun onResponse(
                    call: Call<PacketResponse>,
                    response: Response<PacketResponse>
                ) {
                    if (response.body() != null) {
                        val response: ArrayList<PacketData> =
                            response.body()?.data as ArrayList<PacketData>
                        listPacket.value = response
                    }
                }

                override fun onFailure(call: Call<PacketResponse>, t: Throwable) {
                    Log.e("setPacket", t.message.toString())
                }
            })
    }

    internal fun addTransaction(
        customerName: String,
        qyt: String,
        progressStatus: String,
        paymentStatus: String,
        price: String,
        packetId: String
    ) {
        val apiClient = ApiClient.create()
        apiClient.addTransaction(customerName, qyt, progressStatus, paymentStatus, price, packetId)
            .enqueue(object : Callback<DefaultResponse> {
                override fun onResponse(
                    call: Call<DefaultResponse>,
                    response: Response<DefaultResponse>
                ) {
                    addTransaction.value = response.body()
                }

                override fun onFailure(call: Call<DefaultResponse>, t: Throwable) {
                    Log.e("setAddTransaction", t.message.toString())
                }

            })

    }

    internal fun updateTransaction(
        idTransaction: String,
        customerName: String,
        qyt: String,
        progressStatus: String,
        paymentStatus: String,
        price: String,
        packetId: String
    ) {
        val apiClient = ApiClient.create()
        apiClient.updateTransaction(
            idTransaction,
            customerName,
            qyt,
            progressStatus,
            paymentStatus,
            price,
            packetId
        )
            .enqueue(object : Callback<DefaultResponse> {
                override fun onResponse(
                    call: Call<DefaultResponse>,
                    response: Response<DefaultResponse>
                ) {
                    updateTransaction.value = response.body()
                }

                override fun onFailure(call: Call<DefaultResponse>, t: Throwable) {
                    Log.e("setAddTransaction", t.message.toString())
                }

            })
    }

    internal fun deleteTransaction(
        idTransaction: String
    ) {
        val apiClient = ApiClient.create()
        apiClient.deleteTransaction(
            idTransaction
        )
            .enqueue(object : Callback<DefaultResponse> {
                override fun onResponse(
                    call: Call<DefaultResponse>,
                    response: Response<DefaultResponse>
                ) {
                    updateTransaction.value = response.body()
                }

                override fun onFailure(call: Call<DefaultResponse>, t: Throwable) {
                    Log.e("setAddTransaction", t.message.toString())
                }

            })
    }


    internal fun observeAddTransaction(): LiveData<DefaultResponse> {
        return addTransaction
    }

    internal fun observeUpdateTransaction(): LiveData<DefaultResponse> {
        return updateTransaction
    }

    internal fun observeDeleteTransaction(): LiveData<DefaultResponse> {
        return deleteTransaction
    }

    internal fun observeGetPacket(): LiveData<ArrayList<PacketData>> {
        return listPacket
    }

    internal fun observeGetTransaction(): LiveData<ArrayList<TransactionData>> {
        return listTransaction
    }

}