package com.aminfaruq.laundry.view.packet.viewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aminfaruq.laundry.model.defaultResponse.DefaultResponse
import com.aminfaruq.laundry.model.packet.PacketData
import com.aminfaruq.laundry.model.packet.PacketResponse
import com.aminfaruq.laundry.network.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PacketVM : ViewModel() {

    private val listPacket = MutableLiveData<ArrayList<PacketData>>()
    private val deletePacket = MutableLiveData<DefaultResponse>()
    private val updatePacket = MutableLiveData<DefaultResponse>()
    private val addPacket = MutableLiveData<DefaultResponse>()


    internal fun getPacket() {
        val apiClient = ApiClient.create()
        apiClient.getPacket()
            .enqueue(object : Callback<PacketResponse> {
                override fun onResponse(
                    call: Call<PacketResponse>,
                    response: Response<PacketResponse>
                ) {
                    if (response.body() != null) {
                        val response: ArrayList<PacketData> =
                            response.body()?.data as ArrayList<PacketData>
                        listPacket.value = response
                    }
                }

                override fun onFailure(call: Call<PacketResponse>, t: Throwable) {
                    Log.e("setPacket", t.message.toString())
                }
            })
    }

    internal fun deletePacket(id: String) {
        val apiClient = ApiClient.create()
        apiClient.deletePacket(id)
            .enqueue(object : Callback<DefaultResponse> {

                override fun onResponse(
                    call: Call<DefaultResponse>,
                    response: Response<DefaultResponse>
                ) {
                    deletePacket.value = response.body()
                }

                override fun onFailure(call: Call<DefaultResponse>, t: Throwable) {
                    Log.e("deletedPacket", t.message.toString())
                }
            })
    }

    internal fun updatePacket(id: String, name: String, price: String, note: String) {
        val apiClient = ApiClient.create()
        apiClient.updatePacket(id, name, price, note)
            .enqueue(object : Callback<DefaultResponse>{

                override fun onResponse(
                    call: Call<DefaultResponse>,
                    response: Response<DefaultResponse>
                ) {
                    updatePacket.value = response.body()
                }

                override fun onFailure(call: Call<DefaultResponse>, t: Throwable) {
                    Log.e("updatePacket", t.message.toString())
                }

            })
    }

    internal fun addPacket(name: String, price: String, note: String) {
        val apiClient = ApiClient.create()
        apiClient.addPacket(name, price, note)
            .enqueue(object : Callback<DefaultResponse>{

                override fun onResponse(
                    call: Call<DefaultResponse>,
                    response: Response<DefaultResponse>
                ) {
                    addPacket.value = response.body()
                }

                override fun onFailure(call: Call<DefaultResponse>, t: Throwable) {
                    Log.e("updatePacket", t.message.toString())
                }
            })
    }

    internal fun observeUpdatePacket(): LiveData<DefaultResponse> {
        return updatePacket
    }

    internal fun observeDeletePacket(): LiveData<DefaultResponse> {
        return deletePacket
    }

    internal fun observeAddPacket(): LiveData<DefaultResponse> {
        return addPacket
    }

    internal fun observeGetPacket(): LiveData<ArrayList<PacketData>> {
        return listPacket
    }

}