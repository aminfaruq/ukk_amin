package com.aminfaruq.laundry.model.transaction

data class TransactionResponse (
    val data: MutableList<TransactionData>? = null,
    val message: String? = null,
    val isSuccess: Boolean? = null
)