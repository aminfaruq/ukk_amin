package com.aminfaruq.laundry.view.admin.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aminfaruq.laundry.R
import com.aminfaruq.laundry.model.login.UserData
import kotlinx.android.synthetic.main.item_user.view.*

class AdminAdapter(private val onItemClick: OnItemClick) :
    RecyclerView.Adapter<AdminAdapter.ViewHolder>() {
    private val mData = ArrayList<UserData>()

    fun setData(item: ArrayList<UserData>) {
        mData.clear()
        mData.addAll(item)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val mView = LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false)
        return ViewHolder(mView)
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(mData[position])
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(userItem: UserData) {
            with(itemView) {
                tvName.text = userItem.name
                tvEmail.text = userItem.email
                tvRole.text = userItem.role

                view.setOnClickListener {
                    onItemClick.onClick(userItem)
                }
            }
        }
    }

    interface OnItemClick {
        fun onClick(item: UserData)
    }


}