package com.aminfaruq.laundry.view.transaction.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aminfaruq.laundry.R
import com.aminfaruq.laundry.model.transaction.TransactionData
import kotlinx.android.synthetic.main.item_user.view.*

class TransactionAdapter(private val onItemClick: OnItemClick) :
    RecyclerView.Adapter<TransactionAdapter.ViewHolder>() {

    private val mData = ArrayList<TransactionData>()

    fun setData(item: ArrayList<TransactionData>) {
        mData.clear()
        mData.addAll(item)
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val mView = LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false)
        return ViewHolder(mView)
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(mData[position])
    }

    interface OnItemClick {
        fun onClick(item: TransactionData)
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(userItem: TransactionData) {
            with(itemView) {
                tvName.text = userItem.customer_name
                tvEmail.text = userItem.status_progress
                tvRole.text = userItem.status_payment

                view.setOnClickListener {
                    onItemClick.onClick(userItem)
                }
            }
        }
    }
}