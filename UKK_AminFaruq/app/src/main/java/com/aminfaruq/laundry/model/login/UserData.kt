package com.aminfaruq.laundry.model.login

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserData (
    val password: String? = null,
    val role: String? = null,
    val name: String? = null,
    val id: String? = null,
    val email: String? = null
) : Parcelable {
    data class UserResponse (
        val data: MutableList<UserData>? = null,
        val message: String? = null,
        val isSuccess: Boolean? = null
    )
}