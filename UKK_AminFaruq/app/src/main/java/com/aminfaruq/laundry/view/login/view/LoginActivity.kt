package com.aminfaruq.laundry.view.login.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aminfaruq.laundry.R
import com.aminfaruq.laundry.model.login.UserData
import com.aminfaruq.laundry.view.dashboard.HomeActivity
import com.aminfaruq.laundry.view.login.viewModel.LoginVM
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    companion object {
        var EXTRA_DATA = "extra data"

    }

    private lateinit var loginVM: LoginVM


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        supportActionBar?.hide()
        loginVM = ViewModelProviders.of(this).get(LoginVM::class.java)


        btnLogin.setOnClickListener {
            loginVM.setUser(etEmail.text.toString())
        }

        handleLogin()
    }

    private fun handleLogin(){
        loginVM.getUser().observe(this , Observer { userItem ->
            if (!userItem.isNullOrEmpty()){
                Log.d("LoginActivity", "$userItem")

                if (!userItem.isNullOrEmpty()) {
                    Log.d("LoginActivity", "$userItem")

                    if (userItem[0].password == etPassword.text.toString()) {
                        moveToHome(userItem[0])
                        Toast.makeText(
                            this@LoginActivity,
                            "Berhasil Login",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        Toast.makeText(
                            this@LoginActivity,
                            "Email atau Password salah",
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                } else {
                    //email salah
                    Toast.makeText(
                        this@LoginActivity,
                        "Email atau Password salah",
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("LoginActivity", "data tidak ada")

                }
            }
        })
    }


    private fun moveToHome(user: UserData) {
        val intent = Intent(this, HomeActivity::class.java)
        intent.putExtra(EXTRA_DATA, user)
        startActivity(intent)
    }

}