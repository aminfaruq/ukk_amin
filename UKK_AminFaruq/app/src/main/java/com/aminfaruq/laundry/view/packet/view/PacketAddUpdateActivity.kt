package com.aminfaruq.laundry.view.packet.view

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aminfaruq.laundry.R
import com.aminfaruq.laundry.model.packet.PacketData
import com.aminfaruq.laundry.view.packet.viewModel.PacketVM
import kotlinx.android.synthetic.main.activity_packet_add_update.*

class PacketAddUpdateActivity : AppCompatActivity() , View.OnClickListener {

    private var data: PacketData? = null

    private lateinit var viewModel: PacketVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_packet_add_update)
        data = intent.getParcelableExtra(PacketActivity.EXTRA_DATA)
        viewModel = ViewModelProviders.of(this).get(PacketVM::class.java)

        btnAddUpdate.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        updateUI()
        observable()
    }

    @SuppressLint("SetTextI18n")
    private fun updateUI() {
        if (data != null) {
            etName.setText(data?.name)
            etPrice.setText(data?.price)
            etNote.setText(data?.note)
            btnAddUpdate.text = "Update Paket"
        }else{
            btnAddUpdate.text = "Tambah Paket"
            btnDelete.visibility = View.GONE
        }
    }

    override fun onClick(v: View?) {
        when (v?.id){
            R.id.btnAddUpdate -> {
                if (data == null) {
                    addData()
                } else {
                    updateData()
                }
                finish()
                setResult(RESULT_OK, intent)
            }

            R.id.btnDelete -> {
                viewModel.deletePacket(
                    data?.id.toString()
                )
                finish()
                setResult(RESULT_OK, intent)
            }
            else -> Toast.makeText(this , "have to the something?" , Toast.LENGTH_SHORT).show()
        }
    }

    private fun updateData() {
        viewModel.updatePacket(
            data?.id.toString(),
            etName.text.toString(),
            etPrice.text.toString(),
            etNote.text.toString()
        )
    }

    private fun addData() {
        viewModel.addPacket(
            etName.text.toString(),
            etPrice.text.toString(),
            etNote.text.toString()
        )
    }

    private fun observable(){
        viewModel.observeDeletePacket().observe(this , Observer {
            Toast.makeText(this , it.message , Toast.LENGTH_SHORT).show()
        })

        viewModel.observeUpdatePacket().observe(this , Observer {
            Toast.makeText(this , it.message , Toast.LENGTH_SHORT).show()
        })

        viewModel.observeAddPacket().observe(this , Observer {
            Toast.makeText(this , it.message , Toast.LENGTH_SHORT).show()
        })
    }
}
