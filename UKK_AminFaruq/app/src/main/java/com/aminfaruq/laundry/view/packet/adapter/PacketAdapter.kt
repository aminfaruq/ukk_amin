package com.aminfaruq.laundry.view.packet.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aminfaruq.laundry.R
import com.aminfaruq.laundry.model.packet.PacketData
import kotlinx.android.synthetic.main.item_user.view.*

class PacketAdapter (private val onItemClick: OnItemClick) :
    RecyclerView.Adapter<PacketAdapter.ViewHolder>(){

    private val mData = ArrayList<PacketData>()

    fun setData(item: ArrayList<PacketData>) {
        mData.clear()
        mData.addAll(item)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val mView = LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false)
        return ViewHolder(mView)
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(mData[position])
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(userItem: PacketData) {
            with(itemView) {
                tvName.text = userItem.name
                tvEmail.text = userItem.price
                tvRole.text = userItem.note

                root_view_user.setOnClickListener {
                    onItemClick.onClick(userItem)
                }
            }
        }
    }

    interface OnItemClick {
        fun onClick(item: PacketData)
    }

}