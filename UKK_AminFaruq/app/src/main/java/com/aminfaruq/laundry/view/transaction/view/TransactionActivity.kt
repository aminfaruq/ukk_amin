package com.aminfaruq.laundry.view.transaction.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.aminfaruq.laundry.R
import com.aminfaruq.laundry.model.packet.PacketData
import com.aminfaruq.laundry.model.transaction.TransactionData
import com.aminfaruq.laundry.view.transaction.adapter.TransactionAdapter
import com.aminfaruq.laundry.view.transaction.viewModel.TransactionVM
import kotlinx.android.synthetic.main.activity_transaction.*

class TransactionActivity : AppCompatActivity(), View.OnClickListener {

    companion object {
        var REQ_ADD = 12
        var REQ_UPDATE = 12
        var EXTRA_DATA = "extra_data"
        var EXTRA_PACKET = "extra_packet"
    }

    val listArray = ArrayList<PacketData>()

    private lateinit var transactionVM: TransactionVM
    private lateinit var adapter: TransactionAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transaction)

        transactionVM = ViewModelProviders.of(this).get(TransactionVM::class.java)
        transactionVM.getTransaction()
        transactionVM.getPacket()
        showData()
        observable()
        fab_transaction.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.fab_transaction -> {
                val intent = Intent(
                    this@TransactionActivity,
                    TransactionAddUpdateActivity::class.java
                )
                intent.putParcelableArrayListExtra(EXTRA_PACKET, listArray)
                startActivityForResult(intent, REQ_ADD)
            }
        }
    }

    private fun showData() {
        adapter =
            TransactionAdapter(
                object :
                    TransactionAdapter.OnItemClick {
                    override fun onClick(item: TransactionData) {
                        // your code in here
                        val intent = Intent(
                            this@TransactionActivity,
                            TransactionAddUpdateActivity::class.java
                        )
                        intent.putParcelableArrayListExtra(EXTRA_PACKET, listArray)
                        intent.putExtra(EXTRA_DATA, item)
                        startActivityForResult(intent, REQ_UPDATE)
                    }
                })
        adapter.notifyDataSetChanged()

        rv_transaction.layoutManager = LinearLayoutManager(this)
        rv_transaction.adapter = adapter
    }

    private fun observable() {
        transactionVM.observeGetTransaction().observe(this, Observer {
            transactionVM.getTransaction()
            adapter.setData(it)
        })

        transactionVM.observeGetPacket().observe(this , Observer { item ->
            item.forEach {
                listArray.add(it)
            }
        })
    }


}