package com.aminfaruq.laundry.view.admin.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.aminfaruq.laundry.R
import com.aminfaruq.laundry.model.login.UserData
import com.aminfaruq.laundry.view.admin.adapter.AdminAdapter
import com.aminfaruq.laundry.view.admin.viewModel.AdminVM
import kotlinx.android.synthetic.main.activity_admin.*

class AdminActivity : AppCompatActivity() {

    private lateinit var adminVM: AdminVM
    private lateinit var adapter: AdminAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin)
        adminVM = ViewModelProviders.of(this).get(AdminVM::class.java)
        adminVM.setUser()
        showData()

    }

    private fun showData(){
        adapter = AdminAdapter(object :
            AdminAdapter.OnItemClick {
            override fun onClick(item: UserData) {
                // your code in here
            }
        })
        adapter.notifyDataSetChanged()

        rv_admin.layoutManager = LinearLayoutManager(this)
        rv_admin.adapter = adapter
        adminVM.getUser().observe(this, Observer {
            adapter.setData(it)
        })

    }
}